import os
import errno
import datetime

#move to helpers
def mkdir_p(path):
	try:
		os.makedirs(path)
	except OSError as exc:  # Python >2.5
		if exc.errno == errno.EEXIST and os.path.isdir(path):
			pass
		else:
			raise

'''Todo celery task/cronjob to clear the log with tasks over 3 days old'''
def log(workspace, msg):
	dt = datetime.datetime.utcnow()
	ws_uuid = workspace.split('/')[-2]
	with open('./debug.log', 'a') as logfile:
		_line = "%s | %s | %s\n" % (dt, ws_uuid, msg)
		print _line
		logfile.write(_line)
