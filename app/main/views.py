import os
import json
from flask import render_template, redirect, url_for, session, current_app, flash, Response, send_file, request, jsonify
from mimetypes import MimeTypes
from threading import Thread
from uuid import uuid4
from . import main
from .forms import GenerateMatrixForm, AccessInitForm, AccessQueryForm
from ..helpers import mkdir_p, log

from subprocesses.ttm.prepare_workspace import prepare_shapefiles, MoreThanOneShapefile
from subprocesses.ttm.make_centroids import generate_zone_centroids
from subprocesses.ttm.build_graph import build_graph
from subprocesses.ttm.generate_matrix import generate_matrix
from subprocesses.ttm.teardown import teardown_workspace, clear_workspace
from subprocesses.access.make_access_img import make_access_img
from subprocesses.access.metrics import get_metric_choices
from subprocesses.access.accessibility_processing import generate_accessability_files, NoDataForMetric
from subprocesses.emailing import send_ttm_complete_email, send_failure_email

#todo separate to diff module
import zipfile
from io import BytesIO
import shapefile
import csv

# TODO --> proper 4xx and 5xx error page
# @current_app.errorhandler(404)
# def error_handler(e):
	# return render_template('error.html', error_code=e.code), 404

def get_session_workspace():
	return current_app.config['USER_WORKSPACES'] + '/%s/' % session['uuid']

# todo move to a diff. module?
def generate_ttm_async(workspace, email, matrix_name):
	log(workspace, msg="--------------Start----------------")
	try:
		log(workspace, msg="preparing the workspace...")
		prepare_shapefiles(workspace)
		generate_zone_centroids(workspace)
		log(workspace, msg="Building graph")
		build_graph(workspace)
		log(workspace, msg="Graph built; now generating matrix")
		generate_matrix(workspace)
		log(workspace, msg="Teardown complete; sending TTM email")
		send_ttm_complete_email(workspace, email, name=matrix_name)
		log(workspace, msg="Matrix generated; tearing down workspace")
		teardown_workspace(workspace)
		log(workspace, msg="ttm email sent; removing workspace")
		clear_workspace(workspace)
		log(workspace, msg="Async TTM generation complete!") 
	
	except MoreThanOneShapefile:
		log(workspace, msg="invalid shapefile zip detected (more than one shp or dbf: ambiguous)")
		send_failure_email(workspace, email, reason='more_than_one_shapefile')# todo detect in form validation
		
	except IOError:
		log(workspace, msg="Failed to build and send a travel time matrix")

		with open(os.path.join(workspace, "errors.log"), 'r') as error_log:
			lines = error_log.readlines()
			errors = [line.rstrip('\n') for line in lines]

			if 'TransitTimesException' in errors:
				log(workspace, msg="TransitTimesException; sending failure email")
				send_failure_email(workspace, email, reason='TransitTimesException')

			elif 'GraphNotFoundException' in errors:
				log(workspace, msg="GraphNotFoundException; sending failure email")
				send_failure_email(workspace, email, reason='GraphNotFoundException')

			elif ('MemoryError' in errors) or ('OverflowError' in errors):
				log(workspace, msg="Memory failure; sending failure email")
				send_failure_email(workspace, email, reason='MemoryError')

			else:
				log(workspace, msg="Other error: sending generic failure email")
				send_failure_email(workspace, email, reason='generic', error_info=errors)

	
	log(workspace, msg="-------------Finish----------------")
	
@main.route('/', methods=['GET'])
def index():
	return render_template('index.html')

@main.route('/about', methods=['GET'])
def about():
	return render_template('about.html')

@main.route('/accessibility/results', methods=['GET', 'POST'])
def access_results():
	if not session['posted_files']:
		flash("Accessibility form not completed.")
		return redirect(url_for('.access'))

	workspace = get_session_workspace()

	metric_choices = get_metric_choices(workspace)	

	form = AccessQueryForm()
	form.metric.choices = metric_choices

	show_results=False

	if form.validate_on_submit():
		#set some values
		travel_mode = form.travel_mode.data
		time_threshold = form.time_threshold.data
		metric = form.metric.data

		alias = form.alias.data

		if not alias:
			alias = metric

		try:
			generate_accessability_files(workspace, travel_mode, time_threshold, metric, alias)
			show_results = True

		except NoDataForMetric:
			flash("Couldn't generate data for metric: %s" % alias)
			flash("Check your land use data -- it's possible there is no data for %s" % metric)

	return render_template('access_result.html', form=form, show_results=show_results)


'''move to helpers'''
def is_number(s):
    try:
        float(s)
        return True
    except TypeError:
        return False
    except ValueError:
    	return False

def get_numeric_fields(potential_field_names, zone_identifier, reader):
	remaining = potential_field_names[:]


	atr_names = [name for name in potential_field_names if name != zone_identifier]

	for sr in reader.shapeRecords():
		zone_atrs = dict(zip(potential_field_names, sr.record))

		# valid_attr = (is_number(zone_atrs[name]) or zone_atrs[name] == None or zone_atrs[name] == '')


		for name in atr_names:
			
			valid_attr = (is_number(zone_atrs[name]) or zone_atrs[name] == None or zone_atrs[name] == '')
			
			if (not valid_attr) and name in remaining:
				remaining.remove(name)

			else:
				print "valid: ", zone_atrs[name]

	print "removed: %s" % [name for name in potential_field_names if name not in remaining]

	return remaining

def generate_zone_csv(workspace):
	sf = shapefile.Reader(os.path.join(workspace, "shapefiles", "zones"))

	shp_info_path = os.path.join(workspace, "shp_info.json")
	
	with open(shp_info_path, "rb") as shp_info_file:
		shp_info = json.load(shp_info_file)

	zone_identifier = shp_info['zone_identifier']
	
	# #This will be needed if we use user specify which field is the GEOID (TODO)
	fields = sf.fields[1:]
	potential_field_names = [field[0] for field in fields]

	valid_field_names = get_numeric_fields(potential_field_names, zone_identifier, sf)

	with open(os.path.join(workspace, 'zone_stats.csv'), 'wb') as f:
		writer = csv.writer(f)		

		atr_names = [name for name in valid_field_names if name != zone_identifier]

		header = ["ZONE"] + atr_names

		writer.writerow(header)

		for sr in sf.shapeRecords():
			zone_atrs = dict(zip(potential_field_names, sr.record))

			row = [zone_atrs[zone_identifier]]
			for name in atr_names:
				row.append(zone_atrs[name])
			
			writer.writerow(row)

@main.route('/accessibility', methods=['GET', 'POST'])
def access():
	#todo - move to a decorator
	if 'uuid' not in session.keys():
		session['uuid'] = str(uuid4())
		session['matrix_status'] = 0 		#to be used in a loading bar potentially

	workspace = get_session_workspace()
	clear_workspace(workspace)

	form = AccessInitForm()
	if form.validate_on_submit():
		

		session['posted_files'] = False

		mkdir_p(os.path.join(workspace, 'shapefiles'))
		mkdir_p(os.path.join(workspace, 'output'))

		zone_shapefiles = form.zone_shapefiles.data
		zone_shapefiles.save(os.path.join(workspace, 'shapefiles', 'zones_container.zip'))

		prepare_shapefiles(workspace)

		zone_identifier = form.zone_identifier.data

		shp_info = {
			'zone_identifier': zone_identifier
		}

		shp_info_path = os.path.join(workspace, 'shp_info.json')

		with open(shp_info_path, 'wb') as shp_info_file:
			json.dump(shp_info, shp_info_file)

		ttm_file = form.ttm_file.data
		ttm_file.save(os.path.join(workspace, 'tt_matrix.csv'))	
			
		stats_file_present = form.zones_file.data.filename != ""

		print stats_file_present

		if stats_file_present:
			print "stats file present"
			zone_stats_file = form.zones_file.data
			zone_stats_file.save(os.path.join(workspace, 'zone_stats.csv'))
			print "saved stats file"
		
		else:
			print "no stats file - generating one based on zone shapefile"
			generate_zone_csv(workspace)
			print "generated csv"

		session['posted_files'] = True
		return redirect(url_for('.access_results'))

	return render_template('access.html', form=form)

@main.route('/ttm', methods=['GET', 'POST'])
def ttm():
	#todo - move to a decorator
	if 'uuid' not in session.keys():
		session['uuid'] = str(uuid4())
		session['matrix_status'] = 0 		#to be used in a loading bar potentially

	form = GenerateMatrixForm()
	if form.validate_on_submit():
		#todo need to plan where to save files; need directory tree structure to be planned
		#for now just making sure saving can occur thru form

		workspace = get_session_workspace()	
		clear_workspace(workspace)	

		mkdir_p(os.path.join(workspace, 'shapefiles'))

		gtfs_file = form.gtfs_file.data
		gtfs_file.save(os.path.join(workspace, 'region.gtfs.zip'))

		osm_file = form.osm_file.data
		osm_file.save(os.path.join(workspace, 'region.osm.pbf'))

		zone_shapefiles = form.zone_shapefiles.data
		zone_shapefiles.save(os.path.join(workspace, 'shapefiles', 'zones_container.zip'))		

		zone_identifier = form.zone_identifier.data

		matrix_name = form.matrix_name.data

		#make transit_info.json and save in the workspace
		#include zone_crs and datetime in there

		print str(form.travel_date.data)
		print str(form.travel_time.data)

		transit_info = {
			'zone_identifier': zone_identifier,
			'zone_crs': form.zone_crs.data,
			'travel_date': str(form.travel_date.data),
			'travel_time': str(form.travel_time.data)
		}

		transit_info_path = os.path.join(workspace, 'transit_info.json')

		with open(transit_info_path, 'wb') as transit_info_file:
			json.dump(transit_info, transit_info_file)

		thr = Thread(target=generate_ttm_async, args=[workspace, form.user_email.data, matrix_name])
		thr.start()

		flash("Your travel time matrix is being generated.")
		flash("Go and get a cup of coffee! You should receive an email in a few minutes.")

		session['matrix_status'] = 100

		return redirect(url_for('.index'))

	return render_template('ttm.html', form=form)

@main.route('/fetch/<filename>', methods=['GET'])
def fetch_file(filename):
	fn = os.path.join(get_session_workspace(), 'output', filename)
	
	try:
		with open(fn, 'rb') as fp:
			_data = fp.read()

	except IOError:
		# todo --> raise 4xx error telling user file does not exist
		raise ValueError("Invalid file requested")

	mimetype = MimeTypes().guess_type(filename)[0]

	if mimetype is None:
		mimetype = 'application/octet-stream'

	return Response(
		_data,
		mimetype=mimetype,
		headers={
			'Content-disposition': 'attachment; filename=%s' % filename
		}
	)

'''Todo -- move into sep module'''
def get_shp_attrs(data):
	with zipfile.ZipFile(data, 'r') as _zipfile:
		files = []
		_shp, _dbf = None, None
		for name in _zipfile.namelist():
			# print name
			_ext = name.lower()[-4:]

			if _ext == '.shp':
				_shp = BytesIO(_zipfile.read(name))

			elif _ext == '.dbf':
				_dbf = BytesIO(_zipfile.read(name))

		sf = shapefile.Reader(shp=_shp, dbf=_dbf)

		field_names = [field[0] for field in sf.fields[1:]]

		return field_names

@main.route('/api/dbf_column_choices', methods=['POST'])
def dbf_column_choices():
	try:
		shp_attrs = get_shp_attrs(request.files['formData'])
		resp = jsonify({'attrs': shp_attrs})

		return resp, 200
	except shapefile.ShapefileException:
		return 400 #bad request (didn't contain valid shapefile)