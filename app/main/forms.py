import datetime
from .custom_form_validation import validate_zip_contents, validate_file_type, validate_land_use, validate_crs_in_library, validate_shapefile_contents
from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired #is this in wtforms.validators? 
from wtforms import StringField, FileField, SubmitField, IntegerField, SelectField
from wtforms.validators import Required, regexp, DataRequired, Email, Optional
from wtforms.fields.html5 import DateField
from wtforms_components import TimeField

class ForgivingSelectField(SelectField):

	def pre_validate(self, form):
		pass

class GenerateMatrixForm(FlaskForm):
	gtfs_file = FileField(
		label='1. Upload GTFS Data', 
		validators=[
			FileRequired()
		],
		description="Zip file containing the GTFS data."
	)

	osm_file = FileField(
		label='2. Upload Open Street Map file for region', 
		validators=[
			FileRequired()
		],
		description="Required file extension: .pbf"
	)

	zone_shapefiles = FileField(
		label='3a. Upload zone shapefiles', 
		validators=[
			FileRequired()
		],
		description="Zip file containing at least .dbf and .shp."
	)

	zone_identifier = ForgivingSelectField(
		label='3b. Specify the zone IDs in the shapefile',
		coerce=str,
		choices=(),
		description="The name of the field with the Zone IDs in the .dbf file"
	)

	#in the future we want to only render this if there is no prj file in the zone_shapefiles
	zone_crs = StringField(
		label="4. What is the coordinate reference system of the zone shapefiles?",
		default='4326',
		description="Default: 4326 (WGS 84)"
	)
	
	#maybe we want date and time to be diff fields? see what we can do with this
	travel_date = DateField(
		label="5. Set the day of travel",
		format='%Y-%m-%d',
		default=datetime.date.today()
	)

	travel_time = TimeField(
		label="6. Set the start time of travel",
		default=datetime.time(8, 0, 0)
	)

	user_email = StringField(
		label="7. Enter your email. We'll email you the result in a few minutes.",
		validators=[
			DataRequired(),
			Email()
		],
		description="We will email you from accessibility.uadi@gmail.com - don't forget to check your spam folder!"
	)

	matrix_name = StringField(
		label="8. Specify the name of the CSV file that we send to you",
		default="travel_time_matrix",
		description="(______.csv)"
	)

	submit = SubmitField(
		label='Generate Matrix'
	)

	def validate_gtfs_file(form, field):
		if field.data:
			validate_file_type(field.data, 'zip')

			required_gtfs = set([
				'agency.txt',
				'stops.txt',
				'routes.txt',
				'trips.txt',
				'stop_times.txt',
				'calendar.txt'
			])

			validate_zip_contents(field.data, required_gtfs)

	def validate_osm_file(form, field):
		if field.data:
			validate_file_type(field.data, 'pbf')

	def validate_zone_shapefiles(form, field):
		if field.data:
			validate_file_type(field.data, 'zip')

			required_shp_exts = set([
				'shp',
				'dbf',
			])

			validate_zip_contents(field.data, required_shp_exts, exts=True)
			validate_shapefile_contents(field.data)

	def validate_zone_crs(form, field):
		validate_crs_in_library(field.data)

class AccessInitForm(FlaskForm):
	zone_shapefiles = FileField(
		label='1a. Upload zone shapefiles', 
		validators=[
			FileRequired()
		],
		description="Zip file containing at least .dbf and .shp."
	)

	zone_identifier = ForgivingSelectField(
		label='1b. Specify the zone IDs in the shapefile',
		coerce=str,
		choices=(),
		description="The name of the field with the Zone IDs in the .dbf file"
	)
	
	ttm_file = FileField(
		label="2. Upload Travel Time Matrix for zones", 
		validators=[
			FileRequired()
		],
		description=".csv file representing the travel time matrix for zones in step 1."
	)

	zones_file = FileField(
		label="(Optional) upload Zone Data", 
		validators=[
			# FileRequired(),
			Optional()
		],
		description=".csv file containing the zone data."
	)

	submit = SubmitField(
		label='Submit'
	)

	def validate_zones_file(form, field):
		if field.data:
			validate_file_type(field.data, 'csv')
			validate_land_use(field.data)

	def validate_ttm_file(form, field):
		if field.data:
			validate_file_type(field.data, 'csv')

	def validate_zone_shapefiles(form, field):
		if field.data:
			validate_file_type(field.data, 'zip')

			required_shp_exts = set([
				'shp',
				'dbf',
			])

			validate_zip_contents(field.data, required_shp_exts, exts=True)
			validate_shapefile_contents(field.data)

class AccessQueryForm(FlaskForm):
	# #inbound / outbound travel mode
	travel_mode = SelectField(
		"Travel mode",
		choices=[('outbound', 'outbound'), ('inbound', 'inbound')],
		default='outbound'
	)

	time_threshold = IntegerField(
		label="Time threshold (minutes)",
		default=60
	)

	metric = SelectField(
		"Metric of interest",
		id='select_metric'
	)

	alias = StringField(
		label="Alias for metric",
		description="Optional: give the metric a different, easier to read name."
	)

	submit = SubmitField(
		label='Generate'
	)