import zipfile
import csv
import pyproj
import shapefile
from io import BytesIO
from wtforms.validators import ValidationError

def validate_zip_contents(data, required_set, exts=False):
	with zipfile.ZipFile(data, 'r') as _zipfile:

		if exts:
			# get set of file extensions only
			present_set = set([name.split('.')[-1] for name in _zipfile.namelist() if '.' in name])
			error_msg = "Missing files with the following extension: "
		else:
			# get set of file names
			present_set = set(_zipfile.namelist())
			error_msg = "Missing the following files: "

		# use lowercase
		present_set = set(map(lambda e: e.lower(), present_set))

		if not present_set.issuperset(required_set):
			missing_items = filter(lambda e: e not in present_set, required_set)
			
			for i, item in enumerate(missing_items):
				if i < len(missing_items)-1:
					error_msg += "%s, " % item
				else:
					error_msg += "%s" % item
			
			raise ValidationError(error_msg)

	data.seek(0)

def validate_file_type(data, _type):
	_filename = data.filename
	_content_type = data.headers['Content-Type']

	validation_info = {
		'csv': {
			'ext': '.csv',
			'content_types': [
				'text/plain',
				'text/x-csv',
				'application/vnd.ms-excel',
				'text/csv',
				'application/csv',
				'application/x-csv',
				'text/comma-separated-values',
				'text/x-comma-separated-values',
				'text/tab-separated-values'
			],
			'error_msg': "Expected CSV file."
		},
		'pbf': {
			'ext': '.pbf',
			'content_types': [
				'application/octet-stream'
			],
			'error_msg': "Expected open street map file to be presented in .pbf format."
		},
		'zip': {
			'ext': '.zip',
			'content_types': [
				'application/x-zip-compressed',
				'application/octet-stream',
				'multipart/x-zip',
				'application/zip',
				'application/zip-compressed',
				'application/x-zip-compressed'
			],
			'error_msg': "Expected Zip file"
		}
	}

	valid_file_ext 		= _filename[-4:].lower() == validation_info[_type]['ext']
	valid_content_type 	= _content_type in validation_info[_type]['content_types']

	if not valid_file_ext:
		print "received", _filename[-4:].lower()
		print "expected", validation_info[_type]['ext']

	if not valid_content_type:
		print "received", _content_type
		print "expected", validation_info[_type]['content_types']


	if not (valid_file_ext and valid_content_type):
		raise ValidationError(validation_info[_type]['error_msg'])

def validate_land_use(csv_file):

	print csv_file

	# ensure headers are unique
	# ensure every cell under the header is numeric

	#dynamically get the delimiter
	delimiter 		= csv.Sniffer().sniff(csv_file.read(1024)).delimiter
	csv_file.seek(0)

	reader 			= csv.reader(csv_file, delimiter=delimiter)
	reading_header 	= True

	for row in reader:
		if reading_header:
			reading_header 	= False
			print "length of header", len(row)

			if len(row) > len(set(row)):
				raise ValidationError("Names in header must be unique")

			for _name in row:
				try:
					float(_name)
					print _name, "is a float!!! bad!"
					raise ValidationError("Names in header cannot be numbers")
				
				except ValidationError:
					print "raisin'"
					raise

				except ValueError:
					pass # all good - this means the name isn't numeric

		else:
			for _item in row:
				try:
					float(_item)

				except ValueError:
					raise ValidationError("Data in CSV file must be numeric. Please remove rows with non-numeric data.")

	csv_file.seek(0)

def validate_crs_in_library(crs):
	try:
		user_proj = pyproj.Proj(init="epsg:%s" % crs)
	except RuntimeError:
		raise ValidationError("Could not find a definition for CRS with EPSG code %s" % crs)


def validate_shapefile_contents(data):
	with zipfile.ZipFile(data, 'r') as _zipfile:
		files = []
		_shp, _dbf = None, None
		for name in _zipfile.namelist():
			# print name
			_ext = name.lower()[-4:]

			if _ext == '.shp':
				_shp = BytesIO(_zipfile.read(name))

			elif _ext == '.dbf':
				_dbf = BytesIO(_zipfile.read(name))

		sf = shapefile.Reader(shp=_shp, dbf=_dbf)

		seen_names = {}

		for sr in sf.shapeRecords():
			zone_name = sr.record[0]

			# check for repeated zone IDs.
			if zone_name not in seen_names:
				seen_names[zone_name] = True

			else:
				print "detected repeated zone id: %s" % zone_name
				# data.seek(0)
				# raise ValidationError("Detected repeated zone ID: %d" % zone_name)
	
	data.seek(0)