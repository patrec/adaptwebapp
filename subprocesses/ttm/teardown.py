import os
import shutil

'''
For use after generating TT matrix

TT matrix requires a lot of large files in workspace.
When it's done, we want to clear it asap
(without deleting the TT matrix itself.)
'''
def teardown_workspace(workspace):
	for file in os.listdir(workspace):
		fp = os.path.join(workspace, file)
		print file
		print fp

		if '.' in file:
			if not file == 'tt_matrix.csv':
				os.remove(fp)

		else:
			shutil.rmtree(fp)

'''
Hard clear workspace.

Doesn't always work on windows (hence the try/except)
Not a problem though because server should be linux
'''
def clear_workspace(workspace):
	try:
		shutil.rmtree(workspace)
	except:
		pass