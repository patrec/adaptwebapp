import subprocess
from app.helpers import mkdir_p, log
import os

def generate_matrix(workspace):
	log(workspace, msg="calling jython subprocess to generate matrix")
	subprocess.call([
		'java',
		'-jar',
		'./otp_dependencies/jython-standalone-2.7.0.jar',
		# '-J-XX:-UseGCOverheadLimit',
		# '-J-Xmx10G',
		'-Dpython.path=./otp_dependencies/otp-1.1.0-shaded.jar',
		'./subprocesses/ttm/tt_matrix_generator.py',
		workspace
	])
	log(workspace, msg="Jython subprocess returned")

if __name__ == '__main__':

	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("workspace", type=str, 
		help="Workspace of the user for whom TT is being made")
	args = parser.parse_args()

	generate_matrix(args.workspace)