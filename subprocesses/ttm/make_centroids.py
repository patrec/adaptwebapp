import csv
import pyproj
import re
import shapefile
import os
import json

from shapely.geometry import Polygon
from app.helpers import log

class NonUniqueZoneIds(Exception):
	pass

'''
Calculates centroid via center of mass.
Not sure if this would work for polygons with holes.
Might need to make use of geopandas python package to do that.
'''
def calculate_centroid(shape):
	try:
		coordinates = shape.__geo_interface__['coordinates'][0]
		
		centroid = Polygon(coordinates).centroid
		print "successfully got centroid"
		return (centroid.x, centroid.y)

	except ValueError:
		# Shapely threw a TopologyException 
		# Invalid shape (probably has a self intersection)
		# Use the centre of the shape's bounding box instead
		return bbox_centroid(shape)

'''
"Dumb" method; calculates average location which can be skewed to one side
	of the polygon or even outside of the bounds of the polygon...

Only use this as a backup if shapely doesn't like the polygon that it tried
	to process.

This only occurs if there is a self intersection within the polygon, which 
	is arguably a problem with the supplied shapefiles, not the program itself...
	(i.e. TopologyException)
'''
def bbox_centroid(shape):
	print "Couldn't find proper centroid - using center of bbox instead"

	[x1, y1, x2, y2] = shape.bbox

	x = (x1+x2)/2
	y = (y1+y2)/2

	return (x, y)

def generate_zone_centroids(workspace):
	print "generating centroids"

	sf = shapefile.Reader(os.path.join(workspace, "shapefiles", "zones")) #todo make zones a config setting (no hardcode)

	transit_info_path = os.path.join(workspace, "transit_info.json")
	
	with open(transit_info_path, "rb") as transit_info_file:
		transit_info = json.load(transit_info_file)

	print "got transit info: %s " % str(transit_info)

	user_proj = pyproj.Proj(init="epsg:%s" % transit_info["zone_crs"])


	latlong_proj = pyproj.Proj(proj='latlong', datum='WGS84')

	# #This will be needed if we use user specify which field is the GEOID (TODO)
	# fields = sf.fields[1:]
	# field_names = [field[0] for field in fields]

	log(workspace, msg="made projections; outputting to CSV")

	with open(os.path.join(workspace, 'centroids.csv'), 'wb') as f:
		writer = csv.writer(f)

		header = ["GEOID", "LAT", "LONG"]

		writer.writerow(header)

		seen_names = {}

		for sr in sf.shapeRecords():
			# #This will be needed if we use user specify which field is the GEOID
			# atr = dict(zip(field_names, sr.record))

			if sr.shape.shapeType == 0:
				#null shape - ignore
				print "ignoring null shape"
				continue
			
			try:
				centroid = calculate_centroid(sr.shape)

			except AssertionError:
				# failed assertion means that the supplied shape isn't a polygon (n_points < 3)
				print type(sr)
				print sr.__dict__
				continue

			# convert centroids in user projection to lat long
			centroid_long, centroid_lat = pyproj.transform(user_proj, latlong_proj, *centroid)

			# todo - let user specify which field represents the GEOID as an optional arg
			# for now it has to be record[0] (i.e. first record)
			zone_name = sr.record[0]

			# check for repeated zone IDs.
			if zone_name not in seen_names:
				seen_names[zone_name] = True
				writer.writerow([str(zone_name), centroid_lat, centroid_long])

			else:
				print seen_names
				print zone_name
				# raise NonUniqueZoneIds()
				# TODO --> decide whether we want to completely halt processing when zone IDs aren't unique?
				# 	   --> or do we just want to ignore non-uniques?