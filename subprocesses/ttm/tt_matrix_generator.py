from org.opentripplanner.scripting.api import OtpsEntryPoint
import argparse
import csv
import datetime
import re
import time
import os
import json
import sys

'''
Can't use the app.helpers package because its in a diferent context due to Jython :(
TODO -- find a solution better than code duping	
'''
def log(workspace, msg):
	dt = datetime.datetime.utcnow()
	ws_uuid = workspace.split('/')[-2]

	_logpath = workspace.split('/')[:-3]
	_logpath.append('debug.log')

	log_fp = os.path.join(*_logpath) 	#hack - uses the info about relative path btween logfile and workspaces... 
	
	with open(log_fp, 'a') as logfile:
		_line = "%s | %s | %s\n" % (dt, ws_uuid, msg)
		print _line
		logfile.write(_line)

def write_error(workspace, msg):
	with open(os.path.join(workspace, 'errors.log'), 'a') as error_file:
		print "writing error %s to error log" % msg
		_line = msg + '\n'
		error_file.write(_line)
		print "wrote %s to log." % msg

def generate_matrix(workspace, travel_date, travel_time):


	log(workspace, msg="-------------Jython----------------")
	#todo --> is it possible to do this:
	#			- without supplying graph in mem?
	# 			- just from gtfs??

	# Instantiate an OtpsEntryPoint
	#todo trial and error
	otp = OtpsEntryPoint.fromArgs(['--graphs', workspace, '--router', 'region'])

	log(workspace, msg="Created OTP entry point")

	# TODO is there a way to do it without needing the graph to be pre-built?
	# e.g. can we build the graph in memory from GTFS (eliminating the need for 
	# build_graph)

	# Start timing the code
	start_time = time.time()

	# Get the default router
	router = otp.getRouter('region')

	# Create a default request for a given time
	req = otp.createRequest()

	year, month, day = int(travel_date[:4]), int(travel_date[5:7]), int(travel_date[8:10]) 
	hour, _min, sec = int(travel_time[:2]), int(travel_time[3:5]), 00

	print "\nyear", year, "\nmonth", month, "\nday", day, "\nhour", hour, "\n_min", _min, "\nsec", sec 
	# req.setDateTime(2017, 5, 12, 8, 00, 00)			# set departure time
	req.setDateTime(year, month, day, hour, _min, sec)			# set departure time
	req.setModes('WALK,BUS,RAIL')					# define transport mode

	# Read Points of Destination - The file points.csv contains the columns GEOID, LAT and LONG.
	points = otp.loadCSVPopulation(os.path.join(workspace, 'centroids.csv'), 'LAT', 'LONG')
	dests = otp.loadCSVPopulation(os.path.join(workspace, 'centroids.csv'), 'LAT', 'LONG')


	log(workspace, msg="Loaded centroids as origins and desitinations")

	result_store = {}

	invalid_points = 0
	n_points = 0


	for origin in points:
		n_points += 1
		print "Processing origin: ", origin, " (GEOID: %s)" % origin.getStringData('GEOID')
		result_store[origin.getStringData('GEOID')] = {}
		
		req.setOrigin(origin)
		
		spt = router.plan(req)
		
		if spt is None:
			print "NO PLAN FOR ORIGIN: ", origin.getStringData('GEOID')
			invalid_points += 1
			continue

		# Evaluate the SPT for all points
		result = spt.eval(dests)

		# Add a new row of result in the CSV output
		for r in result:
			# matrixCsv.addRow([ origin.getStringData('GEOID'), r.getIndividual().getStringData('GEOID'), r.getWalkDistance() , r.getTime(),    r.getBoardings() ])
			result_store[origin.getStringData('GEOID')][r.getIndividual().getStringData('GEOID')] = r.getTime()



	log(workspace, msg="Finished creating result store (%d invalid points out of %d)" % (invalid_points, n_points))


	# TODO 	- we are iterating over everything twice as much as needed
	#		- (building a result dict then outputing result dict)
	# 		- It is probably possible to create the .csv directly in the first iter
	# 		- saving both memory and time

	with open(os.path.join(workspace, 'tt_matrix.csv'), 'wb') as f:
		writer = csv.writer(f)

		# we want the header to be a list of zones (where zones are floats)
		# we want the list to be sorted numberically (hence sorted by transforming keys to floats)
		# we want to then convert each zone id to a string, hence the mapping
		header = [""] + list(map(lambda o: str(o), sorted(result_store.keys(), key=lambda zone_id: float(zone_id))))

		writer.writerow(header)

		for origin in sorted(result_store.keys(), key=lambda zone_id: float(zone_id)):
			# print origin
			row = [origin]

			for destination in sorted(result_store.keys(), key=lambda zone_id: float(zone_id)):
				# if destination in result_store[origin].keys()
				# print "\t", destination
				
				if destination in result_store[origin].keys():
					row.append(result_store[origin][destination]/60) #Convert to minutes
				else:
					row.append("-")

			writer.writerow(row)

	log(workspace, msg="Created the matrix file")

	# Stop timing the code
	log(workspace, msg="Elapsed time was %g seconds" % (time.time() - start_time))

	log(workspace, msg="-------------Python----------------")

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("workspace", type=str, 
		help="Workspace of the user for whom TT is being made")
	args = parser.parse_args()

	workspace = args.workspace


	transit_info_path = os.path.join(workspace, "transit_info.json") #todo make workspace agnostic
	
	with open(transit_info_path, "rb") as transit_info_file:
		transit_info = json.load(transit_info_file)


	try:
		generate_matrix(workspace, transit_info['travel_date'], transit_info['travel_time'])
	except MemoryError:
		write_error(workspace, 'MemoryError')
	except OverflowError:
		write_error(workspace, 'OverflowError')

	except:
		#catch all and inspect trace manually
		error_type = str(sys.exc_info()[1])
		if error_type == "org.opentripplanner.routing.error.GraphNotFoundException":
			write_error(workspace, "GraphNotFoundException")

		elif error_type == "org.opentripplanner.routing.error.TransitTimesException":
			write_error(workspace, "TransitTimesException")

		else:
			write_error(workspace, error_type)