import subprocess
from app.helpers import mkdir_p
import os


def build_graph(workspace):
	mkdir_p(os.path.join(workspace, "region"))
	subprocess.call([
		'java',
		'-Xmx10G', 
		'-jar',
		'./otp_dependencies/otp-1.1.0-shaded.jar',
		'--cache',
		# '%s/region' % workspace
		workspace,
		'--basePath',
		# '%s/region' % workspace
		workspace,
		'--build',
		# '%s/region' % workspace
		workspace
	])

	src, dest = (os.path.join(workspace, "Graph.obj"), os.path.join(workspace, "region", "Graph.obj"))
	os.rename(src, dest)