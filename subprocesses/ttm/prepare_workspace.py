import zipfile
import os
import glob

class MoreThanOneShapefile(Exception):
	pass

#unzip zone shapefiles
def prepare_shapefiles(workspace):
	errors_path = os.path.join(workspace, "errors.log")
	os.mknod(errors_path) #create empty file -- REQUIRES ROOT ACCESS ON OSX

	shapefile_path = os.path.join(workspace, 'shapefiles')
	shapes_zipfile = zipfile.ZipFile(os.path.join(shapefile_path, 'zones_container.zip'), 'r')
	shapes_zipfile.extractall(shapefile_path)

	# os.remove(os.path.join(shapefile_path, 'zones.zip'))

	for filename in os.listdir(shapefile_path):
		_, ext = os.path.splitext(filename)
		if ext == ".zip": continue

		new_filename = ('zones%s' % ext).lower()
		print filename, new_filename
		src = os.path.join(shapefile_path, filename)
		dst = os.path.join(shapefile_path, new_filename)

		try:
			os.rename(src, dst)
		except WindowsError:
			raise MoreThanOneShapefile()