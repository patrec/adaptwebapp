import csv
import argparse
import json
import os

class ZoneNotInTTM(Exception):
	pass


def build_outbound_travel_info(desired_zone, threshold, ttm_fn):
	with open(ttm_fn, 'rb') as ttm_file:
		ttm_reader 		= csv.reader(ttm_file)
		header 			= []
		reading_header 	= True

		for row in ttm_reader:
			if reading_header:
				header = row[1:] #first element blank
				header = [float(n) for n in header] # cast all ids to floats
				reading_header = False

			elif str(float(row[0])) == desired_zone:
				#todo get from args
				zone_times = zip(header, row[1:]) #todo cast to int in here w\ list comprehension

				reachable_zone_info = {}

				for _zone_id, travel_time in zone_times:
					zone_id = str(float(_zone_id))
					if travel_time == '-':
						# Means no valid route; don't include this zone 
						# TODO -- expand list of "invalids" to [-, none, NA, N/A, '' etc] ??
						continue

					if int(travel_time) <= threshold:
						reachable_zone_info[zone_id] = int(travel_time)
					
				return reachable_zone_info

		raise ZoneNotInTTM() 	# Got to end of loop without returning -- means zone is not in TTM

#trying to do it without generator...
def build_inbound_travel_info(desired_zone, threshold, ttm_fn):
	with open(ttm_fn, 'rb') as ttm_file:
		ttm_reader 			= csv.reader(ttm_file)
		zone_index 			= None
		reading_header 		= True
		reachable_zone_info = {} #todo better name?

		for row in ttm_reader:
			if reading_header:
				try:
					row = [""] + [str(float(n)) for n in row[1:]] # cast all ids to floats
					zone_index 		= row.index(desired_zone)
					reading_header 	= False
				except ValueError:
					raise ZoneNotInTTM()

			else:
				if row[zone_index] == '-':
					# Means no valid route; don't include this zone 
					continue

				zone_id, travel_time = (str(float(row[0])), int(row[zone_index]))

				if travel_time <= threshold:
					reachable_zone_info[zone_id] = travel_time

		return reachable_zone_info


#dont need this info anymore... refactor
#literally just need list of reachable zones

def zone_travel_time_info(workspace, zone, threshold, inbound=False):
	ztt_info = {
		'origin_zone': zone,
		'travel_direction': ['outbound', 'inbound'][inbound],
		'time_threshold': threshold
	}

	ttm_fn = os.path.join(workspace, 'tt_matrix.csv')

	if inbound:
		ztt_info.update({'zones' : build_inbound_travel_info(zone, threshold, ttm_fn)})
	else:
		ztt_info.update({'zones' : build_outbound_travel_info(zone, threshold, ttm_fn)})

	return ztt_info

def get_surrounding_zones(workspace, zone, threshold, inbound=False):
	zzt_info = zone_travel_time_info(workspace, zone, threshold, inbound)

	try:
		surrounding_zones = zzt_info['zones'].keys()
	except AttributeError:
		#can't get keys of Nonetype --> no zones
		surrounding_zones = []


	return surrounding_zones


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('zone', type=str, help="Zone of interest")
	parser.add_argument('--threshold', type=int, default=30, help="maximum travel time")
	parser.add_argument('--inbound', action='store_true', help="")
	parser.set_defaults(inbound=False)
	args = parser.parse_args()


	zt_info = zone_travel_time_info(args.zone, args.threshold, args.inbound)

	if args.inbound:
		fn = 'zt_info_in.json'
	else:
		fn = 'zt_info_out.json'

	with open(fn, 'wb') as zt_json:
		json.dump(zt_info, zt_json)
