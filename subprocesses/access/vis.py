import os

if not os.name == 'nt':
	#if not windows
	import matplotlib
	matplotlib.use('Agg')
	#needed because matplotlib defaults to Xwindows, causing it to not work on linux server
	#see https://stackoverflow.com/questions/37604289/tkinter-tclerror-no-display-name-and-no-display-environment-variable

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import json
from descartes import PolygonPatch

COLOUR_PERCENT_MAP = {
	(-5, 0): '#ffffff', #white
	(0, 5): '#d3d3d3', #light grey
	(5, 10): '#808080', #dark grey
	(10, 15): '#ffff00', #yellow
	(15, 20): '#00FF00', #green
	(20, 30): '#00FFFF', #baby blue
	(30, 40): '#800080', #light purple
	(40, 50): '#000080', #deep navy blue
	(50, 60): '#FA8072', #salmon
	(60, 80): '#FFA500', #orange
	(80, 100): '#FF0000', #red
}

MONOCHROME_PERCENT_MAP = {
	(-5, 0): '#ffffff', #white
	(0, 5): '#ffe6e6', # light red
	(5, 10): '#ffb3b3', 
	(10, 15): '#ff9999', 
	(15, 20): '#ff8080', 
	(20, 30): '#ff6666', 
	(30, 40): '#ff4d4d', 
	(40, 50): '#ff3333', 
	(50, 60): '#ff1a1a', 
	(60, 80): '#e60000', 
	(80, 100): '#990000', # dark red
}


BLACK = '#000000'


'''
want to read in the zones' coordinates into memory into dictionary mapping zone_id to 
zone_coords

then want to read in the accessibility info and merge with the dict (or maybe instead receive 
already existing access info from previous running of uadi prog.)

Then create matplotlib images based on the zone geography and the new info calculated in
prev step.
'''

class Zone:
	def __init__(self, feature_info, zonal_info, zone_identifier, monochrome):
		self.id 			= feature_info['properties'][zone_identifier]
		self.polygon 		= feature_info['geometry']
		self.metric 		= zonal_info['net_metric']
		self.metric_percent = zonal_info['percent']
		self.monochrome 	= monochrome

	def colour(self):

		if self.monochrome:
			cpm = MONOCHROME_PERCENT_MAP
		else:
			cpm = COLOUR_PERCENT_MAP

		for key in cpm.keys():
			if key[0] <= self.metric_percent < key[1]:
				return cpm[key]

		raise ValueError("couldn't resolve zone (%s) to colour! Metric value: %d" % (self.id, self.metric_percent))

	def polypatch(self):
		return PolygonPatch(self.polygon, fc=self.colour(), ec=BLACK, linewidth=0.05)

def make_legend(ax, monochrome=False):
	patches = []

	if monochrome:
		cpm = MONOCHROME_PERCENT_MAP
	else:
		cpm = COLOUR_PERCENT_MAP

	for _range, _colour in cpm.iteritems():
		if _range == (-5, 0):
			continue
		else:
			patches.append(mpatches.Patch(color=_colour, label='%d%% to %d%%' % (_range[0], _range[1])))

	#this line is needed otherwise there is no guarantee the legend will be ordered 0 -> 100%
	patches = sorted(patches, key=lambda p: int(p._label.split('%')[0]))

	return ax.legend(prop={'size':6}, bbox_to_anchor=(1.15,0.25), handles=patches)

def make_access_img(workspace, access_info, zone_identifier, monochrome=False):
	
	#used this code elsewhere..... turn it into a class method???
	if access_info['travel_direction'] == 'inbound':
		title = "%s catchable within %d minutes by PT (inbound travel)" % (access_info['alias'], access_info['time_threshold'])

	else:
		title = "%s reachable within %d minutes by PT (outbound travel)" % (access_info['alias'], access_info['time_threshold'])

	fig = plt.figure()
	ax = fig.gca()

	with open(os.path.join(workspace, 'shapefiles', 'geo.json'), 'rb') as geojson_file:
		geojson_all = json.load(geojson_file)

		print access_info['zones'].keys()

		for feature_info in geojson_all['features']:
			zone_id = feature_info['properties'][zone_identifier]
			try:
				zonal_info = access_info['zones'][str(float(zone_id))]
			except KeyError:
				print "Couldn't find zonal info for zone %s" % zone_id
				#make dummy zonal info
				zonal_info = {
					'net_metric': 0,
					'percent': -1 		# i.e. don't render a colour from COLOUR_MAP
				}

			zone = Zone(feature_info, zonal_info, zone_identifier, monochrome)
			ax.add_patch(zone.polypatch())

	make_legend(ax, monochrome)

	ax.axis('scaled') #not needed??
	ax.axis('off')
	plt.title(title)

	if monochrome:
		image_fn = 'accessibility-monochrome.png'
	else:
		image_fn = 'accessibility-col.png'


	image_fp = os.path.join(
		workspace,
		'output', 
		image_fn
	)
	
	plt.savefig(image_fp, dpi=2000)
	plt.close()