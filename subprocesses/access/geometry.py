import json
import os
import shapefile
from geoJ.geoJ import GeoJ

def shapefile2geojson(path):
	reader = shapefile.Reader(path)
	fields = reader.fields[1:]
	field_names = [field[0] for field in fields]
	features = []
	for sr in reader.shapeRecords():
		atr = dict(zip(field_names, sr.record))
		geom = sr.shape.__geo_interface__
		features.append(
			dict(
				type="Feature",
				geometry=geom, 
				properties=atr
			)
		)
	
	#below is the geojson format	
	return {
		'type': 'FeatureCollection',
		'features': features
	}


"""
iterate through the geojson
match id attribute to access_info['zones'][zone_id]
pull the metric out using access_info['zones'][zone_id]['net_metric'] and ['percent']
update geojson's singular's ['properties'] with ['properties']['%s'%metric_name] and percentage
write geojson to file
"""
def make_geojson(workspace, access_info, zone_identifier):
	geojson = shapefile2geojson(path=os.path.join(workspace, 'shapefiles', 'zones'))
	metric_name = access_info['metric_name']

	for feature in geojson['features']:
		try:
			feature_id 	= feature['properties'][zone_identifier]
			zone_access_info = access_info['zones'][str(float(feature_id))]
			#does this save???
			feature['properties']['%s_N' % metric_name.upper()] = zone_access_info['net_metric'] 	#n = net
			feature['properties']['%s_P' % metric_name.upper()] = zone_access_info['percent']		#p = percent
		except KeyError:
			feature['properties']['%s_N' % metric_name.upper()] = 0		#n = net
			feature['properties']['%s_P' % metric_name.upper()] = 0		#p = percent

	# note that shapefile .dbf columns are limited to having names being 11 chars long
	# so having %s_P and %s_N COULD POSSIBLY be dangerous if they're already at that limit!!!
	# -_-
	# maybe enforce a restriction that column names have to be 9 chars or less.... (so bad)

	with open(os.path.join(workspace, 'shapefiles', 'geo.json'), 'wb') as geojson_file:
		json.dump(geojson, geojson_file)

def update_shapefile_attrs(workspace):
	#open the geojson made in prev step
	#read into a big ol dict
	#transform big ol dict into a new shapefile
	#save it somewhere distinguishable / just overwrite
	#god i hate shapefiles lol
	#why cant the world just use json EVERYWHERE

	with open(os.path.join(workspace, 'shapefiles', 'geo.json'), 'rb') as geojson_file:
		geojson = json.load(geojson_file)

	new_shapefile_name = 'new_zones'

	#transform geojson --> shp
	gj = GeoJ(os.path.join(workspace, 'shapefiles', 'geo.json'))
	gj.toShp(os.path.join(workspace, 'output', 'zones-altered'))