import os
import csv
import json

def get_metric_choices(workspace):
	#get land use file

	zone_stats_filename = os.path.join(workspace, 'zone_stats.csv')

	with open(zone_stats_filename, 'rb') as zone_stats_csv:
		#dynamically get the delimiter

		try:
			delimiter = csv.Sniffer().sniff(zone_stats_csv.read(1024)).delimiter
			zone_stats_csv.seek(0)
		except csv.Error:
			delimiter = ','
			zone_stats_csv.seek(0)

		if delimiter not in [' ', '\t', '_', ',', ';']:
			delimiter = ','

		print 'delimiter: ', delimiter

		metric_reader = csv.reader(zone_stats_csv, delimiter=delimiter)

		#the header is the choices
		header = metric_reader.next()

		if 'ZONE' in header:
			header.remove('ZONE')

		shp_info_path = os.path.join(workspace, "shp_info.json")
	
		with open(shp_info_path, "rb") as shp_info_file:
			shp_info = json.load(shp_info_file)

		if shp_info['zone_identifier'] in header:
			header.remove(shp_info['zone_identifier'])

		return zip(header, header)