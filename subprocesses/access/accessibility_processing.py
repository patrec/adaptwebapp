import os
import argparse
import json
import csv
from ttm_parser import get_surrounding_zones, ZoneNotInTTM
from vis 		import make_access_img
from geometry 	import make_geojson, update_shapefile_attrs

class NoDataForMetric(Exception):
	pass

#todo supply a lambda on metrics instead of a single metric??
#todo extend for multiple metrics (very possible)
def build_metric_info(zone_stats_file, metric, alias):
	metric_info = {
		'metric_name': metric,
		'metric_total': 0,
		'alias': alias,
		'zones': {}
	}

	with open(zone_stats_file, 'rb') as zone_stats_csv:
		#dynamically get the delimiter
		try:
			delimiter = csv.Sniffer().sniff(zone_stats_csv.read(1024)).delimiter
			zone_stats_csv.seek(0)
		except csv.Error:
			delimiter = ','
			zone_stats_csv.seek(0)

		if delimiter not in [' ', '\t', '_', ',', ';']:
			delimiter = ','

		metric_reader 	= csv.reader(zone_stats_csv, delimiter=delimiter)
		reading_header 	= True
		metric_index 	= None

		for row in metric_reader:
			if reading_header:
				metric_index 	= row.index(metric)
				reading_header 	= False

			else:
				
				zone_id 		= str(float(row[0])) #todo --> this is a hack because zone ids are floats in TTM and ints in LUF...

				try:
					metric_value 	= float(row[metric_index])
					metric_info['metric_total'] += metric_value
					metric_info['zones'][zone_id] = {metric: metric_value}

				except ValueError:
					if row[metric_index] == "":
						pass
						# empty cell -- discard the zone
					else:
						raise

	if metric_info['metric_total'] == 0:
		raise NoDataForMetric()

	return metric_info

def build_accessibility_info(workspace, metric_info, threshold, travel_mode):
	metric_name = metric_info['metric_name']

	access_info = {
		'zones': {},
		'metric_name': metric_name,
		'metric_total': metric_info['metric_total'],
		'travel_direction': ['outbound', 'inbound'][travel_mode=='inbound'],
		'time_threshold': threshold,
		'alias': metric_info['alias']
	}

	for zone_id in metric_info['zones'].keys():

		print "processing zone:", zone_id

		try:
			surrounding_zones = get_surrounding_zones(workspace, zone_id, threshold, inbound=travel_mode=='inbound')
		except ZoneNotInTTM:
			surrounding_zones = []

		net_metric 	= sum([metric_info['zones'][other_zone_id][metric_name] for other_zone_id in surrounding_zones if other_zone_id in metric_info['zones']])

		access_info['zones'][zone_id] = {
			"net_metric": net_metric,
			"percent": net_metric / metric_info['metric_total'] * 100
		}

	# with open(os.path.join(workspace, 'access_info.json'), 'w') as access_info_file:
	# 	json.dump(access_info, access_info_file)

	return access_info

def make_accessibility_table(workspace, access_info):
	accessibility_fn = os.path.join(
		workspace,
		'output',
		'access_table.csv'
	)

	with open(accessibility_fn, 'wb') as accessibility_csv:
		a_writer = csv.writer(accessibility_csv)

		if access_info['travel_direction'] == 'inbound':
			preamble = "%s within %d minutes by PT (inbound travel)" % (access_info['alias'], access_info['time_threshold'])

		else:
			preamble = "%s reachable within %d minutes by PT (outbound travel)" % (access_info['alias'], access_info['time_threshold'])

		a_writer.writerow([preamble])
		a_writer.writerow([])

		table_header = ['zone', '%s (number)' % access_info['metric_name'], '%']
		a_writer.writerow(table_header)

		for zone_id, zone_info in access_info['zones'].iteritems():
			value = zone_info['net_metric']
			percentage = zone_info['percent']
			row = [zone_id, value, percentage]
			a_writer.writerow(row)

def generate_accessability_files(workspace, travel_mode, time_threshold, metric, alias):
	zone_stats_file = os.path.join(workspace, 'zone_stats.csv')

	metric_info = build_metric_info(zone_stats_file, metric, alias)

	access_info = build_accessibility_info(workspace, metric_info, time_threshold, travel_mode)

	shp_info_path = os.path.join(workspace, "shp_info.json")
	
	with open(shp_info_path, "rb") as shp_info_file:
		shp_info = json.load(shp_info_file)

	zone_identifier = shp_info['zone_identifier']

	make_accessibility_table(workspace, access_info)
	make_geojson(workspace, access_info, zone_identifier)
	update_shapefile_attrs(workspace)
	make_access_img(workspace, access_info, zone_identifier)
	make_access_img(workspace, access_info, zone_identifier, monochrome=True)