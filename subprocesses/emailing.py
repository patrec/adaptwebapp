import smtplib
import os
import json
from app.helpers import log
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

with open('./account.json','r') as f:
	acc = json.load(f)
	SENDER 		= acc['ADAPT_EMAIL_USERNAME'].encode('ascii','ignore')
	PASSWORD 	= acc['ADAPT_EMAIL_PASSWORD'].encode('ascii','ignore')

def send_ttm_complete_email(workspace, recipient, name='tt_matrix'):

	_filename = name + '.csv'

	ttm_matrix_fp = os.path.join(workspace, 'tt_matrix.csv')
	receivers = [recipient]

	msg = MIMEMultipart()
	msg['To'] 		= COMMASPACE.join(receivers)
	msg['Date'] 	= formatdate(localtime=True)
	msg['Subject'] 	= "Your travel time matrix has been generated"
	msg['From'] 	= SENDER

	#todo this is getting clipped by gmail. fix somehow?
	#doesnt affect anything really, just would be nice if it didnt do that
	msg_body 		=  "Thank you for your patience! Your travel time matrix has been generated."
	text = MIMEText(msg_body)
	msg.attach(text)

	with open(ttm_matrix_fp, 'rb') as ttm_matrix_file:
		part = MIMEApplication(ttm_matrix_file.read(), Name=_filename)
		part['Content-Disposition'] = 'attachment; filename="%s"' % _filename
		msg.attach(part)

	log(workspace, msg="sending to %s" % receivers)

	try:
		send_email(receivers, msg)
	except smtplib.SMTPException, e:
		print e
		log(workspace, msg="Error: unable to send email to %s" % receivers)

'''General purpose'''
def send_email(receivers, msg):	
	server = smtplib.SMTP('smtp.gmail.com:587')
	# server = smtplib.SMTP_SSL('smtp.`gmail.com', 465)
	server.ehlo()
	server.starttls()
	server.login(SENDER, PASSWORD)
	server.sendmail(SENDER, receivers, msg.as_string())

def send_test_email():
	receivers = ['rglsm7655558@gmail.com']
	msg_body = "Adapt Server Started"
	msg = MIMEMultipart()
	msg['To'] 		= COMMASPACE.join(receivers)
	msg['Date'] 	= formatdate(localtime=True)
	msg['Subject'] 	= msg_body
	msg['From'] 	= SENDER
	text = MIMEText(msg_body)
	msg.attach(text)
	send_email(receivers, msg)

send_test_email()

'''
###
	Todo - get these from static files or even better, html templates!
###
'''
def invalid_transit_times_email():
	msg_body = "The GTFS you supplied does not contain transit data for the travel time that you specified. "
	msg_body += "There is no transit service information available for the time that you specified. "

	return msg_body

def memory_failure_email():
	msg_body =  "Sorry! We ran into an error when generating your travel time matrix. "
	msg_body += "It's not your fault - our server simply ran out of memory. Please try again with a smaller osm.pbf file. "
	msg_body += "We recommend using www.mapzen.com to generate an osm.pbf file for your region of interest."

	return msg_body

def more_than_one_shapefile_email():
	msg_body =  "The .zip containing the shapefile you supplied us contained more than one shapefile. "
	msg_body += "Please supply a new .zip containing the shapefile you want us to use."

	return msg_body

def invalid_graph_email():
	msg_body =  "We weren't able to produce a graph with the open street map file you provided. "
	msg_body += "Please try a different one for the area you are interested in."

	return msg_body

def generic_failure_email(error_info):
	return "Something went wrong, and we aren't exactly sure what. Here's the info: %s" % str(error_info)

def send_failure_email(workspace, recipient, reason, error_info=""):
	print "Subprocess failed - reason: ", reason
	receivers = [recipient]

	msg = MIMEMultipart()
	msg['To'] 		= COMMASPACE.join(receivers)
	msg['Date'] 	= formatdate(localtime=True)
	msg['Subject'] 	= "There was an error generating your travel time matrix"
	msg['From'] 	= SENDER

	if reason == 'MemoryError':
		msg_body = memory_failure_email()
	
	elif reason == 'more_than_one_shapefile':
		msg_body = more_than_one_shapefile_email()

	elif reason == 'TransitTimesException':
		msg_body = invalid_transit_times_email()

	elif reason == 'GraphNotFoundException':
		msg_body = invalid_graph_email()

	elif reason == 'generic':
		msg_body = generic_failure_email(error_info)

	else:
		raise ValueError("Invalid failure reason")

	text = MIMEText(msg_body)
	msg.attach(text)

	log(workspace, msg="sending failure (%s) email to %s" % (reason, receivers))

	try:
		send_email(receivers, msg)
	except smtplib.SMTPException, e:
		print e
		log(workspace, msg="Error: unable to send email to %s" % recipient)