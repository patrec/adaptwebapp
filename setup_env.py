import urllib
import os
import errno

'''
Retrieves OTP dependencies:
1. OTP Package (jar file)
2. Jython standalone executable (for executing OTP)
'''

def mkdir_p(path):
	try:
		os.makedirs(path)
	except OSError as exc:
		if exc.errno == errno.EEXIST and os.path.isdir(path):
			pass
		else:
			raise

mkdir_p('./otp_dependencies')

# Get OTP packages
urllib.urlretrieve('https://repo1.maven.org/maven2/org/opentripplanner/otp/1.1.0/otp-1.1.0-shaded.jar', './otp_dependencies/otp-1.1.0-shaded.jar')

# Get Jython packages
urllib.urlretrieve('http://search.maven.org/remotecontent?filepath=org/python/jython-standalone/2.7.0/jython-standalone-2.7.0.jar', './otp_dependencies/jython-standalone-2.7.0.jar')