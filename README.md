# ADAPT: Automatic Dynamic Accessibility Planning Tool

ADAPT is a Python web application implemented in Flask that allows a user to:

1. Create a travel time matrix for an area using Open Trip Planner (OTP);
2. Use the travel time matrix to perform accessibility analytics.

## Background

### Flask

This Python web application is implemented using the Flask micro-framework,
available from https://flask.pocoo.org/.

Miguel Grinberg's book, "Flask Web Development", is a very good book and I
recommend reading it. Most of the Flask design choices were made are inspired
by the code examples in this book, so you should be able to understand
this project by reading it. The book is available from https://flaskbook.com/.

### Open Trip Planner (OTP)

The travel time matrix is made by calculating the travel time from every zone to
every other zone. For `n` zones, `n^2` trips must be planned. To do this, Open
Trip Planner's scripting API was used. Open Trip Planner is a Java package, but
by using Jython, it was possible to use the Python scripting interface on the
Web server.

## Environment Setup

### System Requirements

The following packages are required to run the ADAPT tool:

* Python 2.7 (`python`) and various packages;
* Pip Installs Packages (`pip`) to install those packages;
* Open Trip Planner and its dependencies (see next section for setup).

### Open Trip Planner (OTP)

This web application relies on the scripting API of Open Trip Planner (OTP). To
set up the environment as well as fetch OTP and its dependencies, run the
`setup_env.py` script by issuing the command:

> python setup_env.py

### Python packages

#### Linux or macOS

To install the required Python packages on Linux or macOS, simply run:

> pip install -r requirements-linux.txt

A future release will remove the Windows-flavoured code which will mean only a
single `requirements.txt` file is needed.

#### Windows

To install the required Python packages on Windows, Shapely must first be
manually installed. This is because the Shapely package delivered by `pip` does
not work on Windows.

Before installing Shapely, you must first determine whether you are running
32-bit or 64-bit Python. Open your console (`cmd`) and issue the command:

> python

You should see a result similar to:

> Python 2.7.12 (v2.7.12:d33e0cf91556, Jun 27 2016, 15:19:22) [MSC v.1500 32 bit (Intel)] on win32
> Type "help", "copyright", "credits" or "license" for more information.

The result indicates which version of Python is being run (in this case,
`2.7.12`) and whether it is 32-bit or 64-bit (in this case, 32-bit from
`win32`).

**IMPORTANT: While your operating system might be 64-bit, it is possible that
your Python installation might be 32-bit! It is important to check.**

Once you know these details, visit
https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely and choose a wheel (`.whl`)
that matches your Python installation.

In the case above, you would download `Shapely-1.5.17-cp27-cp27m-win32.whl`
as Python 2.7 (32-bit) is installed.

Once Shapely is installed, issue the following command to install the rest of
the Python dependencies:

> pip install -r requirements.txt

## Usage

### Local Deployment

To run the application locally, issue the command:

> python manage.py runserver

To use the tool, visit the website by navigating in your Web browser to
`localhost:5000` (as port 5000 is the Flask default).

### Remote Deployment

To run the application on a server, such that it can be accessed remotely:

> nohup python manage.py runserver --host=0.0.0.0 &

The tool can now be accessed from `http://<IP-ADDRESS-OF-SERVER>:5000`, where
`<IP-ADDRESS-OF-SERVER>` is the publicly accessible IPv4 address of the server.

The server code will be running in a background process. You will be able to
interact with the server (such as run `git pull` to update the code) without
taking down the Web server. While running in a background process, the server
logs will be saved to `nohup.out`. To view the `nohup.out` logs, issue the
command:

> tail -50 nohup.out

which will output the last 50 lines of logged events on the server.

To stop the server running the Web application, you will need to kill the
background process listening on port 5000. To do this, issue the command:

> fuser -k -n tcp 5000

**WARNING: It is strongly recommended that the 'Remote Deployment' method is
improved upon in the future. This very naive method to deploy the web server
due to the original authors' extremely limited experience in server deployment
and lack of time to learn how to properly deploy (or receive help from the
university).**

Before deploying this Web application into production, it is recommended that
you set up a reverse proxy server like `nginx`. Due to the memory-intensive
computation involved with Open Trip Planner, it is recommended to use
cloud computing services to deploy instances of the application with a load
balancer.

## Contacts

Please contact Chao Sun or Tristan Reed at the University of Western Australia
for queries about this software. Thanks is given to Yan Ji and Callan Bright,
alongside the UADI project team and Arup Pty Ltd for their work on the ADAPT
system and its forks.
